return {
  current = 'erlandsona',
  schemes = {
    BreezeDark = {
      foreground = '#eff0f1',
      background = '#31363b',
      ansi = { '#31363b','#ed1515','#11d116','#f67400','#1d99f3','#9b59b6','#1abc9c','#eff0f1' },
      brights = { '#7f8c8d','#c0392b','#1cdc9a','#fdbc4b','#3daee9','#8e44ad','#16a085','#fcfcfc' },

      -- The color of the split lines between panes
      split = "#2e3436", -- Hiding the line because there's transparency delineating the panes.
      -- Overrides the cell background color when the current cell is occupied by the
      -- cursor and the cursor style is set to Block
      cursor_bg = "#222",
      -- Overrides the text color when the current cell is occupied by the cursor
      cursor_fg = "chartreuse",
      -- Specifies the border color of the cursor when the cursor style is set to Block,
      -- of the color of the vertical or horizontal bar when the cursor style is set to
      -- Bar or Underline.
      cursor_border = "yellow",

      -- the foreground color of selected text
      selection_fg = "darkviolet",
      -- the background color of selected text
      selection_bg = "#222",

      -- The color of the scrollbar "thumb"; the portion that represents the current viewport
      scrollbar_thumb = "green",
    },
    BreezeLight = {
      foreground = '#383a42',
      -- foreground_faint = '#44424c'
      -- foreground_intense = '#1c1a24'
      background = '#fafafa',
      -- background_faint = '#e6e6e6'
      -- background_intense = '#fff'

      ansi = {'#383a42', '#e4564a', '#50a050', '#c5bd60', '#0070d4', '#a626a4', '#0a96b4', '#fafafa' },
      -- brights = { '#1c1a24', '#f83636', '#50c850', '#de6602', '#005ce8', '#ba0eb8', '#00aac8', '#fff' }
      brights = { '#44424c', '#c6605e', '#648c64', '#ae9802', '#1484c0', '#885488', '#1eaaa0', '#e6e6e6' },

      -- The color of the split lines between panes
      split = "#2e3436", -- Hiding the line because there's transparency delineating the panes.
      -- Overrides the cell background color when the current cell is occupied by the
      -- cursor and the cursor style is set to Block
      cursor_bg = "silver",
      -- Overrides the text color when the current cell is occupied by the cursor
      cursor_fg = "chartreuse",
      -- Specifies the border color of the cursor when the cursor style is set to Block,
      -- of the color of the vertical or horizontal bar when the cursor style is set to
      -- Bar or Underline.
      cursor_border = "yellow",

      -- the foreground color of selected text
      selection_fg = "darkviolet",
      -- the background color of selected text
      selection_bg = '#AEAEAE',


      -- The color of the scrollbar "thumb"; the portion that represents the current viewport
      scrollbar_thumb = "green",
    },
    erlandsona = {
      -- Inspired by Breeze Dark but w/ a Yellow foreground
      -- The default text color
      foreground = '#c5bd60',
      -- foreground = "olive",
      -- The default background color
      background = '#2e3436',
      ansi = { '#2b2a2a', '#b21818', '#4e9a06', '#f57900', '#3465a4', '#b218b2', '#18b2b2', '#b2b2b2' },
      brights = { '#686868', '#ff5454', '#8ae234', '#ce5c00', '#729fcf', '#ff54ff', '#54ffff', '#ffffff' },

      -- The color of the split lines between panes
      split = "#2e3436", -- Hiding the line because there's transparency delineating the panes.
      -- Overrides the cell background color when the current cell is occupied by the
      -- cursor and the cursor style is set to Block
      cursor_bg = "#222",
      -- Overrides the text color when the current cell is occupied by the cursor
      cursor_fg = "chartreuse",
      -- Specifies the border color of the cursor when the cursor style is set to Block,
      -- of the color of the vertical or horizontal bar when the cursor style is set to
      -- Bar or Underline.
      cursor_border = "yellow",

      -- the foreground color of selected text
      selection_fg = "darkviolet",
      -- the background color of selected text
      selection_bg = "#222",

      -- The color of the scrollbar "thumb"; the portion that represents the current viewport
      scrollbar_thumb = "green",
    }
  }
}
