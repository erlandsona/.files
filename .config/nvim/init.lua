cmd = vim.cmd  -- to execute Vim commands e.g. cmd('pwd')
fn = vim.fn    -- to call Vim functions e.g. fn.bufnr()
let = vim.g      -- a table to access global variables
set = vim.o

require'settings'

let.mapleader = ','


-- packer.nvim automatic install
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  cmd 'packadd packer.nvim'
end

--- {{{ Plugins
require('packer').startup{
  -- Packer manages itself
  { 'wbthomason/packer.nvim'

  , { 'svermeulen/vimpeccable' , config = require'config.mappings' } -- syntactic sugar for vimrc in lua

  --  'vimwiki/vimwiki';
  --  'romainl/vim-cool';
  --  't9md/vim-choosewin';
  --  'qpkorr/vim-bufkill';
  --  'airblade/vim-rooter';
  --  'jiangmiao/auto-pairs';
  --  'junegunn/rainbow_parentheses.vim';

  , { 'nvim-treesitter/nvim-treesitter'
    , run = ":TSUpdate"
    , config = function()
        -- require 'nvim-treesitter.install'.compilers = {'gcc'}
        require'nvim-treesitter.configs'.setup(require'config.treesitter')
      end
    }

  , { 'tjdevries/colorbuddy.nvim' -- Colorschemes in lua
    , requires = {'nvim-treesitter/nvim-treesitter', opt = true}
    }
  , { 'rktjmp/lush.nvim'
    , config = function()
        -- ansi colors
        local wezterm = require('wezterm.colors')
        local colors = wezterm.schemes[wezterm.current]
        let.terminal_fg = colors.foreground
        let.terminal_bg = colors.background
        let.terminal_color_0 = colors.ansi[1]
        let.terminal_color_1 = colors.ansi[2]
        let.terminal_color_2 = colors.ansi[3]
        let.terminal_color_3 = colors.ansi[4]
        let.terminal_color_4 = colors.ansi[5]
        let.terminal_color_5 = colors.ansi[6]
        let.terminal_color_6 = colors.ansi[7]
        let.terminal_color_7 = colors.ansi[8]
        -- bright colors
        let.terminal_color_8 = colors.brights[1]
        let.terminal_color_9 = colors.brights[2]
        let.terminal_color_10 = colors.brights[3]
        let.terminal_color_11 = colors.brights[4]
        let.terminal_color_12 = colors.brights[5]
        let.terminal_color_13 = colors.brights[6]
        let.terminal_color_14 = colors.brights[7]
        let.terminal_color_15 = colors.brights[8]
        cmd[[
          function! SynGroup()
              let l:s = synID(line('.'), col('.'), 1)
              echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
          endfun
          function! SynStack()
            if !exists("*synstack")
              return
            endif
            echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
          endfunc
        ]]
        require'lush'(require'highlight')

        -- highlight line 80 and 120+
        -- cmd'hi! ColorColumn ctermbg=black guibg=black'
        cmd'let &colorcolumn="80,".join(range(120,999),",")'

        -- 80 Column - Not working because it conflicts with vim-better-whitespace
        -- hi OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
        -- function! OverLength()
        --   3match OverLength /\%>80v.\+/
        -- endfunction

        -- call OverLength()
        -- augroup OverLength
        --   autocmd!
        --   autocmd BufEnter,BufWrite
        --         \,CursorMoved,CursorMovedI
        --         \,InsertEnter,InsertLeave
        --         \ cs
        --         \,elm,haskell
        --         \,javascript,javascript.jsx,js
        --         \,json
        --         \,php
        --         \,python
        --         \,ruby
        --         \,vim
        --         \ call OverLength()
        -- augroup END

        -- cmd'colorscheme erlandsona'
        require'statusline'
      end
    }
  --
  -- , { 'sunjon/shade.nvim'
  --   , config = function()
  --       require'shade'.setup
  --         { overlay_opacity = 50
  --         , opacity_step = 1
  --         , keys =
  --           { brightness_up    = '<C-Up>'
  --           , brightness_down  = '<C-Down>'
  --           , toggle           = '<Leader>s'
  --           }
  --         }
  --     end
  --   }

  , { 'norcalli/nvim-colorizer.lua' -- Highlight CSS Colors like Hex codes and such.
    , config = function() require'colorizer'.setup() end
    }

  -- , { 'lukas-reineke/indent-blankline.nvim'
  --   , config = function()
  --       let.indent_blankline_show_first_indent_level = false
  --       let.indent_blankline_use_treesitter = true
  --       let.indent_blankline_char = '│'
  --     end
  --   }

  -- TODO: Include lspsage for more well formatted error reporting.
  , { 'kabouzeid/nvim-lspinstall'
    , requires = {{'svermeulen/vimpeccable'}, {'neovim/nvim-lspconfig'}}
    , config = require'config.lsp'
    }

  , { 'hrsh7th/nvim-compe'
    , config = function() require'compe'.setup(require'config.compe') end
    }

  -- TODO: learn 'mg979/vim-visual-multi';
  , 'terryma/vim-multiple-cursors'

  -- TODO: Try windwp/nvim-spectre, or bqf.vim
  , 'stefandtw/quickfix-reflector.vim' -- Make edits to entries in the quickfix window

  -- {{{ FZF Replacement
  -- TODO: Waiting on https://github.com/nvim-telescope/telescope.nvim/issues/416
  , 'nvim-lua/popup.nvim'
  , 'nvim-lua/plenary.nvim'

  -- {{{ Telescope Config
  --   { 'nvim-telescope/telescope.nvim'
  --   , requires = {{'svermeulen/vimpeccable'}}
  --   , config = require'config.telescope'
    -- }
  -- }}}

  , { 'junegunn/fzf'
    , dir = '~/.fzf'
    , run = './install'
    , requires = {{'junegunn/fzf.vim'}, {'svermeulen/vimpeccable'}}
    , config = function()
        local vimp = require'vimp'
        -- nnoremap <c-t> :tags<cr>
        vimp.nnoremap({'override'}, '<c-p>', ':Gfiles -co --exclude-per-directory=.gitignore<CR>')
        vimp.nnoremap({'override'}, '<c-\\>', ':Rg <c-r><c-w><CR>')
        vimp.vnoremap({'override'}, '<c-\\>', '"vy:Rg <c-r>v<CR>')
        vimp.nnoremap({'override'}, '<leader><bslash>', ':Rg<Space>')

        -- override ag and rg commands to search inside git repo and add preview
        cmd[[command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, fzf#vim#with_preview({'dir': FugitiveWorkTree()}), <bang>0)]]
        cmd[[command! -bang -nargs=* Rg call fzf#vim#grep("rg --multiline --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, fzf#vim#with_preview({'dir': FugitiveWorkTree()}), <bang>0)]]

        -- override gfiles to add preview
        cmd[[command! -bang -nargs=? Gfiles call fzf#vim#gitfiles('-co --exclude-per-directory=.gitignore', fzf#vim#with_preview(), <bang>0)]]
      end
    }
-- }}}

  -- TODO: Encorporate jesseduffield/lazygit & kdheepak/lazygit.nvim for easier change management.
  , { 'lewis6991/gitsigns.nvim'
    , requires = 'nvim-lua/plenary.nvim'
    , config = function() require'gitsigns'.setup(require'config.gitsigns') end
    }
  , 'tpope/vim-fugitive' -- :Git blame

-- {{{ NerdTree
-- 'kyazdani42/nvim-tree.lua'; -- https://github.com/kyazdani42/nvim-tree.lua/issues/200
  , { 'dhruvasagar/vim-vinegar'
    , as = 'nerd-vinegar'
    , requires = {{'preservim/nerdtree'}, {'kyazdani42/nvim-web-devicons'}}
    , config = function()
        -- let NERDTreeChDirMode = 0 -- Don't change Vim's CWD
        let.NERDTreeHijackNetrw = 1
        let.NERDTreeHighlightCursorline = 1
        let.NERDTreeIgnore = { 'DS_Store' }
        let.NERDTreeMouseMode = 3 -- Single click any node to open.
        let.NERDTreeShowHidden = 1
        let.NERDTreeShowLineNumbers = 0
        let.NERDTreeCascadeOpenSingleChildDir = 1
        let.NERDTreeAutoDeleteBuffer = 1
      end
    }
-- }}}

  , 'tpope/vim-abolish'
  , 'tpope/vim-commentary'
  , 'tpope/vim-surround' -- TODO: Use lua alternative: 'blackCauldron7/surround.nvim'
  , 'tpope/vim-repeat'

  -- Might be slow...
  , 'Raimondi/delimitMate' -- ' => '' -- TODO: replace with treesitter / compe?

  -- for :DB postgres://etc... running sql queries from vim...
  --  'tpope/vim-dadbod'

  -- Blocks / Text Objects
  -- 'nvim-treesitter/nvim-treesitter-refactor';
  , {'nvim-treesitter/playground', opt = true}
  -- 'code-biscuits/nvim-biscuits'

  , { 'andymass/vim-matchup'
    , event = 'VimEnter'
    , config = function()
        let['matchup_matchparen_status_offscreen'] = 0
      end
    }


  --  'tpope/vim-projectionist'
  --  'thaerkh/vim-workspace'
  --  'airblade/vim-rooter'
  , { 'dhruvasagar/vim-prosession'
    , requires = {{'tpope/vim-obsession'}}
    -- , config = require'config.sessions'
    }
    --  'gikmx/vim-ctrlposession'

  , 'wellle/targets.vim' -- Extra Text Objects
  , {'kana/vim-textobj-user', opt = true}
  , {'kana/vim-textobj-indent', opt = true}
  , {'kana/vim-textobj-line', opt = true}
  , {'andyl/vim-textobj-elixir', opt = true}
  , {'Julian/vim-textobj-variable-segment', opt = true} -- av/iv for camelCase or snake_case variables...
  , {'arithran/vim-delete-hidden-buffers'}

  -- Doesn't seem to shade the background and opens in new window which is confusing?
  -- , 'folke/zen-mode.nvim'
  -- , 'Pocco81/TrueZen.nvim' -- Also an interesting take...
  , { 'troydm/zoomwintab.vim'
    , requires = {{'svermeulen/vimpeccable'}}
    , config = function()
        let.zoomwintab_hidetabbar = 0
        require'vimp'.nnoremap({ 'silent', 'override' }, '<C-z>', [[:ZoomWinTabToggle<CR>]])
      end
    }

  , 'ntpeters/vim-better-whitespace'
  , {'amadeus/vim-convert-color-to', opt = true}
  }
}
-- }}}

-- This is OSX Fullscreen :thumbsdown: :(
-- let.neovide_fullscreen = true
-- vim:foldmethod=marker:foldlevel=0
