-- cmd'syntax on'
-- cmd'colorscheme peachpuff'
--
--        ,gggg,
--       d8" "8I                         ,dPYb,
--       88  ,dP                         IP'`Yb
--    8888888P"                          I8  8I
--       88                              I8  8'
--       88        gg      gg    ,g,     I8 dPgg,
--  ,aa,_88        I8      8I   ,8'8,    I8dP" "8I
-- dP" "88P        I8,    ,8I  ,8'  Yb   I8P    I8
-- Yb,_,d88b,,_   ,d8b,  ,d8b,,8'_   8) ,d8     I8,
--  "Y8P"  "Y888888P'"Y88P"`Y8P' "YY8P8P88P     `Y8
--
-- This is the Lush tutorial. It demostrates the functionality of Lush and how
-- to write a basic lush-spec. For more information, see the README and :h lush.
--
-- A Lush theme starter template can be found in the examples folder.
--

-- First, enable lush.ify on this file, run:
--
--  `:Lushify`
--
--  or
--
--  `:lua require('lush').ify()`
--
-- (try putting your cursor inside the ` and typing yi`:@"<CR>)
-- (make sure to enable termguicolors with `set termguicolors`)
--
-- Calls to hsl()/hsluv() are now highlighted with the correct background colour
-- Highlight names groups will have the highlight style applied to them.

-- Lets get started, first we have to require lush, and optionally bind
-- hsl to a more usable name. HSL can also be imported into other modules.

local lush = require('lush')
local hsluv = lush.hsluv
local hsl = lush.hsl
-- local rgb = lush.rgb
local let = let
local set = set


-- local black  = hsluv(0, 0, 0)
-- local white = hsluv(0, 100, 100)
-- local red = hsluv(10, 100, 50)
-- local orange = hsluv(30, 100, 50)
-- local yellow = hsluv(60, 100, 50)
-- local lime = hsluv(90, 100, 50)
-- local green = hsluv(120, 100, 50)
-- local teal = hsluv(180, 100, 50)
-- local sky = hsluv(210, 100, 50)
-- local blue = hsluv(240, 100, 50)
-- local purple = hsluv(270, 100, 50)
-- local pink = hsluv(300, 100, 50)
-- local rose = hsluv(330, 100, 50)


local black  = hsl(0, 0, 0)  -- 0
-- local black  = rgb(0, 0, 0)  -- 0

local red = hsl(0, 100, 50) -- 1

-- Green
local green = hsl(120, 100, 50) -- 2
local lime = hsl(90, 100, 50) -- 9

-- Yellows
local orange = hsl(30, 100, 50) -- 3
local yellow = hsl(60, 100, 50) -- 10

-- Blues
local cyan = hsl(180, 100, 50) -- 11
local azure = hsl(210, 100, 50) -- 4
local blue = hsl(230, 100, 50) -- 4

local purple = hsl(263, 100, 50) -- 5
local pink = hsl(300, 100, 50) -- 12


local dark_grey  = hsl(0, 0, 10)  -- 8
local light_grey  = hsl(0, 0, 90)  -- 14

-- local teal = hsluv(180, 100, 50) -- 6
-- local sky = hsluv(210, 100, 50) -- 13
-- local rose = hsl(330, 100, 50)

local white = hsl(0, 100, 100) -- 7

local widen = function()
end

local theme = lush(function()
  set.background='dark'
  let.colors_name='erlandsona'

  return {
    -- peachpuff
    --
    --
    --
    --

    -- " Vim color file
    -- " Maintainer: David Ne\v{c}as (Yeti) <yeti@physics.muni.cz>
    -- " Last Change: 2003-04-23
    -- " URL: http://trific.ath.cx/Ftp/vim/colors/peachpuff.vim

    -- " This color scheme uses a peachpuff background (what you've expected when it's
    -- " called peachpuff?).
    -- "
    -- " Note: Only GUI colors differ from default, on terminal it's just `light'.

    -- " First remove all existing highlighting.
    -- set background=light
    -- hi clear
    -- if exists("syntax_on")
    --   syntax reset
    -- endif

    -- let colors_name = "peachpuff"

    -- Normal { guibg=PeachPuff, guifg=Black }
    Normal {},

    -- SpecialKey { term=bold ctermfg=4 guifg=Blue }
    SpecialKey { fg=let.terminal_color_4, gui='bold' },

    NonText { fg=let.terminal_color_4, gui='bold' },

    ---- NERDTree
    -- Directory { term=bold ctermfg=4 guifg=Blue }
    Directory { fg=let.terminal_color_4, gui='bold'  },

    ErrorMsg { gui='bold'--,standout
             , fg=let.terminal_color_7
             , bg=let.terminal_color_1
             -- gui versions
             -- , fg='White'
             -- , bg='Red'
            },
    IncSearch { gui='reverse' },

    -- Search { term=reverse ctermbg=3 guibg=Gold2 },
    Search { gui='underline,bold', bg='#222222' },

    MoreMsg { fg=let.terminal_color_2
            , gui='bold'
            -- gui
            -- , fg='SeaGreen'
            },
    ModeMsg { gui='bold' },

    -- LineNr { term=underline ctermfg=3 guifg=Red3 },
    LineNr { fg=let.terminal_color_5
           },
    Question { fg=let.terminal_color_2
             , gui='bold'
             -- , guifg='SeaGreen'
             },

    -- StatusLine { term='bold',reverse cterm=bold,reverse gui=bold guifg=White guibg=Black },
    StatusLine { fg=let.terminal_fg },
    -- StatusLineNC { term=reverse cterm=reverse gui=bold guifg=PeachPuff guibg=Gray45 },
    StatusLineNC { fg=let.terminal_fg },

    -- VertSplit { term=reverse cterm=reverse gui=bold guifg=White guibg=Gray45 },
    VertSplit { fg=let.terminal_fg },

    Title { gui='bold'
          , fg=let.terminal_color_5
          -- , guifg='DeepPink3'
          },

    ---- {{{ Highlight Links
    multiple_cursors_cursor { Search },
    multiple_cursors_visual { Search },

    -- Visual { term=reverse cterm=reverse gui=reverse guifg=Grey80 guibg=fg },
    Visual { Search },
    ---- }}}

    WarningMsg { gui='bold'
               , fg=let.terminal_color_1
               -- , guifg='Red'
               },
    WildMenu { gui='bold'
             , fg=let.terminal_color_0
             , bg=let.terminal_color_3
             -- , guifg='Black'
             -- , guibg='Yellow'
             },

    -- Folded { term='standout' ctermfg=4 ctermbg=7 guifg=Black guibg='#e3c1a5' },
    Folded { fg=let.terminal_fg },

    FoldColumn { fg=let.terminal_color_4
               , bg=let.terminal_color_7
               -- , gui='standout'
               -- , guifg='DarkBlue'
               -- , guibg='Gray80'
               },

    -- DiffAdd { term=bold ctermbg=4 guibg=White },
    DiffAdd { fg=let.terminal_color_10
            , bg='green'
            },
    -- DiffChange { term=bold ctermbg=5 guibg=#edb5cd },
    DiffChange { fg=let.terminal_color_13
               , bg=let.terminal_fg
               , gui='bold'
               },
    -- DiffDelete { term=bold cterm=bold ctermfg=4 ctermbg=6 gui=bold guifg=LightBlue guibg=#f6e8d0 },
    DiffDelete { fg=let.terminal_color_9
               , bg='red'
               , gui='bold'
               },
    -- DiffText { term=reverse cterm=bold ctermbg=1 gui=bold guibg=#ff8060 },
    DiffText { fg=let.terminal_bg
             , bg=let.terminal_color_6
             , gui='bold'
             },

    -- Cursor { guifg=bg guibg=fg },
    CursorLine { gui='underline'
               , fg='bg'
               , bg='fg'
               },
    CursorLineNr { fg=let.terminal_color_11 },
    -- lCursor { guifg='bg', guibg=fg },

    -- " Colors for syntax highlighting
    Comment { gui='bold,italic'
            , fg=let.terminal_color_4
            -- , fg='#406090'
            },
    -- Comment { fg=blue, gui='italic' },

    Constant { fg=let.terminal_color_1
             -- , gui='italic'
             -- , guifg='#c00058'
             },
    Special { gui='bold'
            , fg=let.terminal_color_5
            -- , guifg='SlateBlue'
            },
    Identifier { gui='bold'
               , fg=let.terminal_color_6
               -- , guifg='DarkCyan'
               },
    Statement { gui='bold'
              , fg=let.terminal_color_3
              -- , guifg='Brown'
              },
    PreProc { gui='bold'
            , fg=let.terminal_color_5
            -- , guifg='Magenta3'
            },
    Type { gui='bold'
         , fg=let.terminal_color_2
         -- , guifg='SeaGreen'
         },
    Ignore { gui='bold'
           , fg=let.terminal_color_7
           -- , guifg=bg
           },
    Error { gui='bold,reverse'
          , fg=let.terminal_color_7
          , bg=let.terminal_color_1
          -- , guifg=White
          -- , guibg=Red
          },
    Todo { fg=let.terminal_color_0
         , bg=let.terminal_color_3
         -- , term=standout
         -- , guifg=Blue
         -- , guibg=Yellow
         },




    -- erlandsona Overrides

    ---- Tabline
    TabLineSel { gui='bold,underline' }, -- Active Tab
    TabLine {}, -- Inactive Tabss
    TabLineFill {}, -- Gap between tabs and [X] on right.



    ---- PopUp Menus
    Pmenu      { bg='#202020' },
    PmenuSel   { bg='#272727' },
    PmenuSbar  { bg='Grey' },
    PmenuThumb { bg='Black' },


    SignColumn {},

    GitSignsChange { fg='#bbbb00', bg=let.terminal_bg },
    GitSignsAdd { fg='#008000', bg=let.terminal_bg },
    GitSignsDelete { fg='#ff0000', bg=let.terminal_bg },

    -- highlight line 80 and 120+
    ColorColumn { bg='Black' },















    -- NormalFloat  { }, -- Normal text in floating windows.
    -- ColorColumn  { }, -- used for the columns set with 'colorcolumn'
    -- Conceal      { }, -- placeholder characters substituted for concealed text (see 'conceallevel')
    -- Cursor       { }, -- character under the cursor
    -- lCursor      { }, -- the character under the cursor when |language-mapping| is used (see 'guicursor')
    -- CursorIM     { }, -- like Cursor, but used when in IME mode |CursorIM|
    -- Directory    { }, -- directory names (and other special names in listings)
    -- DiffAdd      { }, -- diff mode: Added line |diff.txt|
    -- DiffChange   { }, -- diff mode: Changed line |diff.txt|
    -- DiffDelete   { }, -- diff mode: Deleted line |diff.txt|
    -- DiffText     { }, -- diff mode: Changed text within a changed line |diff.txt|
    -- EndOfBuffer  { }, -- filler lines (~) after the end of the buffer.  By default, this is highlighted like |hl-NonText|.
    -- TermCursor   { }, -- cursor in a focused terminal
    -- TermCursorNC { }, -- cursor in an unfocused terminal
    -- ErrorMsg     { }, -- error messages on the command line
    -- VertSplit    { }, -- the column separating vertically split windows
    -- Folded       { }, -- line used for closed folds
    -- FoldColumn   { }, -- 'foldcolumn'
    -- SignColumn   { }, -- column where |signs| are displayed
    -- Substitute   { }, -- |:substitute| replacement text highlighting
    -- MatchParen   { }, -- The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
    -- ModeMsg      { }, -- 'showmode' message (e.g., "-- INSERT -- ")
    -- MsgArea      { }, -- Area for messages and cmdline
    -- MsgSeparator { }, -- Separator for scrolled messages, `msgsep` flag of 'display'
    -- MoreMsg      { }, -- |more-prompt|
    -- NonText      { }, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
    -- NormalNC     { }, -- normal text in non-current windows
    -- Pmenu        { }, -- Popup menu: normal item.
    -- PmenuSel     { }, -- Popup menu: selected item.
    -- PmenuSbar    { }, -- Popup menu: scrollbar.
    -- PmenuThumb   { }, -- Popup menu: Thumb of the scrollbar.
    -- Question     { }, -- |hit-enter| prompt and yes/no questions
    -- QuickFixLine { }, -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
    -- SpecialKey   { }, -- Unprintable characters: text displayed differently from what it really is.  But not 'listchars' whitespace. |hl-Whitespace|
    -- SpellBad     { }, -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
    -- SpellCap     { }, -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
    -- SpellLocal   { }, -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
    -- SpellRare    { }, -- Word that is recognized by the spellchecker as one that is hardly ever used.  |spell| Combined with the highlighting used otherwise.
    -- StatusLine   { }, -- status line of current window
    -- StatusLineNC { }, -- status lines of not-current windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
    -- TabLine      { }, -- tab pages line, not active tab page label
    -- TabLineFill  { }, -- tab pages line, where there are no labels
    -- TabLineSel   { }, -- tab pages line, active tab page label
    -- Title        { }, -- titles for output from ":set all", ":autocmd" etc.
    -- Visual       { }, -- Visual mode selection
    -- VisualNOS    { }, -- Visual mode selection when vim is "Not Owning the Selection".
    -- WarningMsg   { }, -- warning messages
    -- Whitespace   { }, -- "nbsp", "space", "tab" and "trail" in 'listchars'
    -- WildMenu     { }, -- current match in 'wildmenu' completion

    -- These groups are not listed as default vim groups,
    -- but they are defacto standard group names for syntax highlighting.
    -- commented out groups should chain up to their "preferred" group by
    -- default,
    -- Uncomment and edit if you want more specific syntax highlighting.

    -- Constant       { }, -- (preferred) any constant
    -- String         { }, --   a string constant: "this is a string"
    -- Character      { }, --  a character constant: 'c', '\n'
    -- Number         { }, --   a number constant: 234, 0xff
    -- Boolean        { }, --  a boolean constant: TRUE, false
    -- Float          { }, --    a floating point constant: 2.3e10

    -- Identifier     { }, -- (preferred) any variable name
    -- Function       { }, -- function name (also: methods for classes)

    -- Statement      { }, -- (preferred) any statement
    -- Conditional    { }, --  if, then, else, endif, switch, etc.
    -- Repeat         { }, --   for, do, while, etc.
    -- Label          { }, --    case, default, etc.
    -- Operator       { }, -- "sizeof", "+", "*", etc.
    -- Keyword        { }, --  any other keyword
    -- Exception      { }, --  try, catch, throw

    -- PreProc        { }, -- (preferred) generic Preprocessor
    -- Include        { }, --  preprocessor #include
    -- Define         { }, --   preprocessor #define
    -- Macro          { }, --    same as Define
    -- PreCondit      { }, --  preprocessor #if, #else, #endif, etc.

    -- Type           { }, -- (preferred) int, long, char, etc.
    -- StorageClass   { }, -- static, register, volatile, etc.
    -- Structure      { }, --  struct, union, enum, etc.
    -- Typedef        { }, --  A typedef

    -- Special        { }, -- (preferred) any special symbol
    -- SpecialChar    { }, --  special character in a constant
    -- Tag            { }, --    you can use CTRL-] on this
    -- Delimiter      { }, --  character that needs attention
    -- SpecialComment { }, -- special things inside a comment
    -- Debug          { }, --    debugging statements

    -- Underlined { gui = "underline" }, -- (preferred) text that stands out, HTML links
    -- Bold       { gui = "bold" },
    -- Italic     { gui = "italic" },

    -- ("Ignore", below, may be invisible...)
    -- Ignore         { }, -- (preferred) left blank, hidden  |hl-Ignore|

    -- Error          { }, -- (preferred) any erroneous construct

    -- Todo           { }, -- (preferred) anything that needs extra attention; mostly the keywords TODO FIXME and XXX

    -- These groups are for the native LSP client. Some other LSP clients may use
    -- these groups, or use their own. Consult your LSP client's documentation.

    -- LspDiagnosticsError               { }, -- used for "Error" diagnostic virtual text
    -- LspDiagnosticsErrorSign           { }, -- used for "Error" diagnostic signs in sign column
    -- LspDiagnosticsErrorFloating       { }, -- used for "Error" diagnostic messages in the diagnostics float
    -- LspDiagnosticsWarning             { }, -- used for "Warning" diagnostic virtual text
    -- LspDiagnosticsWarningSign         { }, -- used for "Warning" diagnostic signs in sign column
    -- LspDiagnosticsWarningFloating     { }, -- used for "Warning" diagnostic messages in the diagnostics float
    -- LspDiagnosticsInformation         { }, -- used for "Information" diagnostic virtual text
    -- LspDiagnosticsInformationSign     { }, -- used for "Information" signs in sign column
    -- LspDiagnosticsInformationFloating { }, -- used for "Information" diagnostic messages in the diagnostics float
    -- LspDiagnosticsHint                { }, -- used for "Hint" diagnostic virtual text
    -- LspDiagnosticsHintSign            { }, -- used for "Hint" diagnostic signs in sign column
    -- LspDiagnosticsHintFloating        { }, -- used for "Hint" diagnostic messages in the diagnostics float
    -- LspReferenceText                  { }, -- used for highlighting "text" references
    -- LspReferenceRead                  { }, -- used for highlighting "read" references
    -- LspReferenceWrite                 { }, -- used for highlighting "write" references

    -- These groups are for the neovim tree-sitter highlights.
    -- As of writing, tree-sitter support is a WIP, group names may change.
    -- By default, most of these groups link to an appropriate Vim group,
    -- TSError -> Error for example, so you do not have to define these unless
    -- you explicitly want to support Treesitter's improved syntax awareness.

    -- TSError              { }, -- For syntax/parser errors.
    -- TSPunctDelimiter     { }, -- For delimiters ie: `.`
    -- TSPunctBracket       { }, -- For brackets and parens.
    -- TSPunctSpecial       { }, -- For special punctutation that does not fall in the catagories before.
    -- TSConstant           { }, -- For constants
    -- TSConstBuiltin       { }, -- For constant that are built in the language: `nil` in Lua.
    -- TSConstMacro         { }, -- For constants that are defined by macros: `NULL` in C.
    -- TSString             { }, -- For strings.
    -- TSStringRegex        { }, -- For regexes.
    -- TSStringEscape       { }, -- For escape characters within a string.
    -- TSCharacter          { }, -- For characters.
    -- TSNumber             { }, -- For integers.
    -- TSBoolean            { }, -- For booleans.
    -- TSFloat              { }, -- For floats.
    -- TSFunction           { }, -- For function (calls and definitions).
    -- TSFuncBuiltin        { }, -- For builtin functions: `table.insert` in Lua.
    -- TSFuncMacro          { }, -- For macro defined fuctions (calls and definitions): each `macro_rules` in Rust.
    -- TSParameter          { }, -- For parameters of a function.
    -- TSParameterReference { }, -- For references to parameters of a function.
    -- TSMethod             { }, -- For method calls and definitions.
    -- TSField              { }, -- For fields.
    -- TSProperty           { }, -- Same as `TSField`.
    -- TSConstructor        { }, -- For constructor calls and definitions: `                                                                       { }` in Lua, and Java constructors.
    -- TSConditional        { }, -- For keywords related to conditionnals.
    -- TSRepeat             { }, -- For keywords related to loops.
    -- TSLabel              { }, -- For labels: `label:` in C and `:label:` in Lua.
    -- TSOperator           { }, -- For any operator: `+`, but also `->` and `*` in C.
    -- TSKeyword            { }, -- For keywords that don't fall in previous categories.
    -- TSKeywordFunction    { }, -- For keywords used to define a fuction.
    -- TSException          { }, -- For exception related keywords.
    -- TSType               { }, -- For types.
    -- TSTypeBuiltin        { }, -- For builtin types (you guessed it, right ?).
    -- TSNamespace          { }, -- For identifiers referring to modules and namespaces.
    -- TSInclude            { }, -- For includes: `#include` in C, `use` or `extern crate` in Rust, or `require` in Lua.
    -- TSAnnotation         { }, -- For C++/Dart attributes, annotations that can be attached to the code to denote some kind of meta information.
    -- TSText               { }, -- For strings considered text in a markup language.
    -- TSStrong             { }, -- For text to be represented with strong.
    -- TSEmphasis           { }, -- For text to be represented with emphasis.
    -- TSUnderline          { }, -- For text to be represented with an underline.
    -- TSTitle              { }, -- Text that is part of a title.
    -- TSLiteral            { }, -- Literal text.
    -- TSURI                { }, -- Any URI like a link or email.
    -- TSVariable           { }, -- Any variable name that does not have another highlight.
    -- TSVariableBuiltin    { }, -- Variable names that are defined by the languages, like `this` or `self`.
  }
end)

-- export-external
--
-- Integrating Lush with other tools:
--
-- By default, lush() actually returns your theme in parsed form. You can
-- interact with it in much the same way as you can inside a lush-spec.
--
-- This looks something like:
--
--   local theme = lush(function()
--     return {
--       Normal { fg = hsl(0, 100, 50) },
--       CursorLine { Normal },
--     }
--   end)
--
--   theme.Normal.fg()                     -- returns table {h = h, s = s, l = l}
--   tostring(theme.Normal.fg)             -- returns "#hexstring"
--   tostring(theme.Normal.fg.lighten(10)) -- you can still modify colours, etc
--
-- This means you can `require('my_lush_file')` in any lua code to access your
-- themes's color information.
--
-- Note:
--
-- "Linked" groups do not expose their colours, you can find the key
-- of their linked group via the 'link' key (may require chaining)
--
--   theme.CursorLine.fg() -- This is bad!
--   theme.CursorLine.link   -- = "Normal"
--
-- Also Note:
--
-- Most plugins expose their own Highlight groups, which *should be the primary
-- method for setting theme colours*, there are however some plugins that
-- require adjustments to a global or configuration variable.
--
-- To set a global variable, use neovims lua bridge,
--
--   vim.g.my_plugin.color_for_widget = theme.Normal.fg.hex
--
-- An example of where you may use this, might be to configure Lightline. See
-- the examples folder for two styles of this.
--
-- Exporting your theme beyond Lush:
--
-- If you wish to use your theme in Vim, or without loading lush, you may export
-- your theme via `lush.export_to_buffer(parsed_lush_spec)`. The readme has
-- further details on how to do this.

-- return our parsed theme for extension or use else where.
return theme

-- vi:nowrap:cursorline:number

