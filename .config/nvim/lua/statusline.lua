local set = vim.o

cmd'hi! Modified  guifg=red ctermfg=red'
cmd'hi! BaseColor guibg=none ctermbg=none'

local mode_colors = {
	['n'] = { gui = let.terminal_color_4, term = 'darkblue' },
	-- ['no'] = 'n·operator pending ',
	['i'] = { gui = let.terminal_color_5, term = 'darkmagenta' },
	['R'] = { gui = let.terminal_color_1, term = 'darkred' },
	['Rv'] = { gui = let.terminal_color_1, term = 'darkred' },
	['v'] = { gui = let.terminal_color_2, term = 'darkgreen' },
	['V'] = { gui = let.terminal_color_2, term = 'darkgreen' },
	[''] = { gui = let.terminal_color_2, term = 'darkgreen' },
	-- ['s'] = 'select ',
	-- ['S'] = 's·line ',
	-- [''] = 's·block ',
	-- ['c'] = 'command ',
	-- ['cv'] = 'vim ex ',
	-- ['ce'] = 'ex ',
  -- Fill these in?
	-- ['r'] = 'prompt ',
	-- ['rm'] = 'more ',
	-- ['r?'] = 'confirm ',
	-- ['!'] = 'shell ',
	-- ['t'] = 'terminal '
}

function RedrawStatuslineColors()
  local color = mode_colors[vim.api.nvim_get_mode().mode] or {gui = let.terminal_color_0, term = 'black'}
  local bg = 7 -- grey
  cmd('hi! PathColor guibg='..color.gui..' ctermbg='..color.term)
  cmd('hi! IconColor guibg='..bg..' guifg='..color.gui..' ctermbg='..bg..' ctermfg='..color.term)
end

-- local function StatusLineEnter()
--   RedrawStatuslineColors()
--   local statusline = "%#BaseColor# %#PathColor#%#PathColor#%< %f %#IconColor#"
--   local statusline .= &mod==1 ? "%#Modified#%m" : ""
-- end
cmd[[function! StatusLineEnter()
  lua RedrawStatuslineColors()

  let statusline=""
  let statusline.="%#BaseColor# %#PathColor#%#PathColor#%< %f %#IconColor#"
  let statusline.=&mod==1 ? "%#Modified#%m" : ""
  let statusline.="%R%*"

  if exists('b:term_title')
    return ""
  else
    return statusline
  endif
endfunction]]

cmd[[function! StatusLineLeave()
  return "%#Modified#%m%#StatusLine#"
endfunction]]

set.statusline='%!StatusLineEnter()'
cmd[[
augroup statusline
  autocmd!
  au WinEnter * setlocal statusline=%!StatusLineEnter()
  au WinLeave * setlocal statusline=%!StatusLineLeave()
augroup END
]]
