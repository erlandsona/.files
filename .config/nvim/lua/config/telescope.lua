return function()
  require('telescope').setup{
    defaults = {
      vimgrep_arguments = {
        'rg',
        '--color=never',
        '--no-heading',
        '--with-filename',
        '--line-number',
        '--column',
        '--smart-case'
      },
      prompt_position = "bottom",
      prompt_prefix = "> ",
      selection_caret = "> ",
      entry_prefix = "  ",
      initial_mode = "insert",
      selection_strategy = "reset",
      sorting_strategy = "descending",
      layout_strategy = "horizontal",
      layout_defaults = {
        horizontal = {
          mirror = false,
        },
        vertical = {
          mirror = false,
        },
      },
      file_sorter =  require'telescope.sorters'.get_fuzzy_file,
      file_ignore_patterns = {},
      generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
      shorten_path = true,
      winblend = 0,
      width = 0.75,
      preview_cutoff = 120,
      results_height = 1,
      results_width = 0.8,
      border = {},
      borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
      color_devicons = true,
      use_less = true,
      set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
      file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
      grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
      qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,

      -- Developer configurations: Not meant for general override
      buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker
    }
  }

  local vimp = require'vimp'
  -- Find files using Telescope
  vimp.nnoremap({'override'}, '<c-p>', require('telescope.builtin').find_files)
  vimp.nnoremap({'override'}, '<c-\\>', function() require('telescope.builtin').grep_string{search = fn.expand('<cword>')} end)
  -- TODO: Replace stringy impl with lua function: https://github.com/neovim/neovim/pull/13896
  vimp.vnoremap({'override'}, '<c-\\>', '"vy:Telescope grep_string search=<c-r>v')
  -- function()
    --   require('telescope.builtin').grep_string{search = fn.expand('<cword>')}
    -- end)
    -- vimp.nnoremap({'override'}, '<leader>fb', require('telescope.builtin').buffers)
    vimp.nnoremap({'override'}, '<leader><bslash>', require('telescope.builtin').live_grep)
    vimp.nnoremap({'override'}, '<leader>h', require('telescope.builtin').help_tags)
  end
