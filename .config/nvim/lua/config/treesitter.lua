return {
  -- ensure_installed = "maintained", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  ensure_installed = {
    -- Shells
    "bash",
    -- Config
    "json", "yaml", "dockerfile",
    -- Langs
    "elm", "lua", "python",
    -- Web
    "html", "css", "javascript", "scss", "graphql",
    -- Wat?
    "comment", "regex"
  },
  -- ignore_install = { "javascript" }, -- List of parsers to ignore installing

  -- playground
  query_linter = require'config.playground',
  highlight = { enable = true },
  autotag = { enable = true },
  matchup = { enable = true },
  -- Replace matchup for better `%` jumping!
  -- pairs = {
    --   enable = true,
    --   disable = {},
    --   highlight_pair_events = {}, -- e.g. {"CursorMoved"}, -- when to highlight the pairs, use {} to deactivate highlighting
    --   highlight_self = false, -- whether to highlight also the part of the pair under cursor (or only the partner)
    --   goto_right_end = false, -- whether to go to the end of the right partner or the beginning
    --   fallback_cmd_normal = "call matchit#Match_wrapper('',1,'n')", -- What command to issue when we can't find a pair (e.g. "normal! %")
    --   keymaps = { goto_partner = "%" }
    -- }
  }
