return {
  enable = true,
  use_virtual_text = true,
  lint_events = {"BufWrite", "CursorHold"},
}
