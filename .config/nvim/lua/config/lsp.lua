return function()
  -- use tab for trigger completion with characters ahead and navigate.
  -- note: use command ':verbose imap <tab>' to make sure tab is not mapped by
  -- other plugin before putting this into your config.
  -- inoremap <silent><expr> <tab>
  --       \ pumvisible() ? "\<c-n>" :
  --       \ <sid>check_back_space() ? "\<tab>" :
  --       \ coc#refresh()
  -- inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<c-h>"

  local lspinstall = require'lspinstall'
  local vimp = require'vimp'
  -- lspinstall.install_server('elm')
  local function setup_servers()
    lspinstall.setup()
    local servers = lspinstall.installed_servers()
    local lsp = vim.lsp

    -- Use a loop to conveniently call 'setup' on multiple servers and
    -- map buffer local keybindings when the language server attaches
    for _, server in pairs(servers) do
      require'lspconfig'[server].setup{
        -- Use an on_attach function to only map the following keys
        -- after the language server attaches to the current buffer
        on_attach = function(_, bufnr)
          -- local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end)
          -- local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

          --Enable completion triggered by <c-x><c-o>
          vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

          -- Mappings.
          -- local opts = { noremap=true, silent=true }

          -- See `:help vim.lsp.*` for documentation on any of the below functions
          vimp.add_buffer_maps(bufnr, function()
            local function nnoremap(...) vimp.nnoremap({'silent', 'override'}, ...) end

            -- Hover Shift/Ctrl-K
            nnoremap('K', lsp.buf.hover)
            nnoremap('<C-k>', lsp.buf.signature_help)
            nnoremap('<leader>S', lsp.buf.code_action)

            -- GOTO: g[D,d,i]
            nnoremap('gD', lsp.buf.declaration)
            nnoremap('gd', lsp.buf.definition)
            nnoremap('gi', lsp.buf.implementation)
            -- nnoremap('<space>D', lsp.buf.type_definition)

            nnoremap('<leader>r', lsp.buf.references)
            nnoremap('<leader>n', lsp.buf.rename)

            nnoremap('<leader>=', lsp.buf.formatting)

            -- Workspace Folders???
            -- nnoremap('<space>wa', lsp.buf.add_workspace_folder)
            -- nnoremap('<space>wr', lsp.buf.remove_workspace_folder)
            -- nnoremap('<space>wl', function() print(vim.inspect(lsp.buf.list_workspace_folders())) end)

            -- Diagnostic Info: AKA: elm-analyze -ish...
            nnoremap('<space>q', lsp.diagnostic.set_loclist)
            nnoremap('<space>e', lsp.diagnostic.show_line_diagnostics)
            nnoremap('[d', lsp.diagnostic.goto_prev)
            nnoremap(']d', lsp.diagnostic.goto_next)
          end)

          cmd('autocmd BufWritePre <buffer=' .. bufnr .. '> lua vim.lsp.buf.formatting_sync(nil, 5000)') -- 5 seconds
        end
      }
    end
  end
  cmd[[autocmd FileType elm setlocal shiftwidth=4 tabstop=4]]

  setup_servers()

  -- Automatically reload after `:LspInstall <server>` so we don't have to restart neovim
  require'lspinstall'.post_install_hook = function ()
    setup_servers() -- reload installed servers
    cmd'bufdo e' -- this triggers the FileType autocmd that starts the server
  end
end
