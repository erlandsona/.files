return function()
  local vimp = require'vimp'

  vimp.nnoremap({'override'}, ';', ':')

  -- r = reload vimrc plugin
  -- vimp.nnoremap('<leader>r', function()
  --   -- Remove all previously added vimpeccable maps
  --   vimp.unmap_all()
  --   -- Unload the lua namespace so that the next time require('vimrc') or require('vimrc.X') is called
  --   -- it will reload the file
  --   -- By default, require() will only load the lua file the first time it is called and thereafter
  --   -- pull it from a cache
  --   unload_lua_namespace('')
  --   -- Make sure all open buffers are saved
  --   cmd('silent wa')
  --   -- Execute our vimrc lua file again to add back our maps
  --   cmd'source ~/.config/nvim/init.lua'

  --   print("Reloaded vimrc!")
  -- end)


  -- require('plenary.reload').reload_module('.', true)
  --
  -- tnoremap <M-z> <C-\><C-n>:ZoomWinTabToggle<CR>i


  -- {{{ Vim-Tmux-Navigator:
  -- `let g:tmux_navigator_disable_when_zoomed = 1` -- Causes slow pane switching
  -- TmuxAwareNavigate is a refactor based on this .vimrc
  cmd[[
  function! TmuxAwareNavigate(to)
    let current_vim_pane = winnr()
    execute 'wincmd ' . a:to
    if current_vim_pane == winnr()
      " The socket path is the first value in the comma-separated list of $TMUX.
      let socket = split($TMUX, ',')[0]
      let selected_pane = 'select-pane -'.tr(a:to, 'phjkl', 'lLDUR')
      let cmd = 'tmux -S '.socket.' '.selected_pane
      return system(cmd)
      silent call s:TmuxCommand(args)
    endif
  endfunction
  ]]

  vimp.nnoremap({ 'silent', 'override' }, '<M-h>', ":call TmuxAwareNavigate('h')<CR>")
  vimp.nnoremap({ 'silent', 'override' }, '<M-j>', ":call TmuxAwareNavigate('j')<CR>")
  vimp.nnoremap({ 'silent', 'override' }, '<M-k>', ":call TmuxAwareNavigate('k')<CR>")
  vimp.nnoremap({ 'silent', 'override' }, '<M-l>', ":call TmuxAwareNavigate('l')<CR>")
  vimp.inoremap({ 'silent', 'override' }, '<M-h>', "<Esc>:call TmuxAwareNavigate('h')<CR>")
  vimp.inoremap({ 'silent', 'override' }, '<M-j>', "<Esc>:call TmuxAwareNavigate('j')<CR>")
  vimp.inoremap({ 'silent', 'override' }, '<M-k>', "<Esc>:call TmuxAwareNavigate('k')<CR>")
  vimp.inoremap({ 'silent', 'override' }, '<M-l>', "<Esc>:call TmuxAwareNavigate('l')<CR>")
  -- nnoremap <silent> <BS> :TmuxNavigateLeft<CR>
  -- nnoremap <Esc>\ :TmuxNavigatePrevious<CR>
  -- }}}

  -- {{{ Reset Pane Layout
  vimp.nnoremap({ 'silent', 'override' }, '=', ':wincmd =<CR>')
  -- }}}

  -- {{{ Clear highlighting on escape in normal mode
  vimp.nnoremap({ 'silent', 'override' }, '<leader>/', ':noh<CR>')
  -- }}}

  -- {{{ Set control-d/u to only scroll 5 lines
  vimp.nnoremap({'override'}, '<c-u>', '5k')
  vimp.nnoremap({'override'}, '<c-d>', '5j')
  vimp.nnoremap({'override'}, '<c-b>', '20k')
  vimp.nnoremap({'override'}, '<c-f>', '20j')
  vimp.vnoremap({'override'}, '<c-u>', '5k')
  vimp.vnoremap({'override'}, '<c-d>', '5j')
  vimp.vnoremap({'override'}, '<c-b>', '20k')
  vimp.vnoremap({'override'}, '<c-f>', '20j')
  -- }}}

  -- {{{ Set mouse scrolling more sensible.
  vimp.rbind('niv', {'override'}, '<ScrollWheelUp>', '<c-y>')
  vimp.rbind('niv', {'override'}, '<ScrollWheelDown>', '<c-e>')
  -- }}}

  -- {{{ Search for selected text, forwards or backwards.
  vimp.vnoremap({ 'silent', 'override' }, '*', [[:<C-U>let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>gvy/<C-R><C-R>=substitute(escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>gV:call setreg('"', old_reg, old_regtype)<CR>]])
  vimp.vnoremap({ 'silent', 'override' }, '#', [[:<C-U>let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>gvy?<C-R><C-R>=substitute(escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>gV:call setreg('"', old_reg, old_regtype)<CR>]])
  -- }}}
end
