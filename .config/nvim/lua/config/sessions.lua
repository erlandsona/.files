" Plug 'thaerkh/vim-workspace'
" Plug 'airblade/vim-rooter'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-obsession'
  Plug 'dhruvasagar/vim-prosession'
    " Plug 'gikmx/vim-ctrlposession'


augroup sessions
  autocmd!
  autocmd TermOpen * setlocal norelativenumber nomodified nonumber wrap
  autocmd BufEnter * if &buftype == 'terminal' | :startinsert | endif
augroup END

" For getting out of insert more in a :terminal buffer.
tnoremap <Esc> <C-\><C-n>

nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
inoremap <A-h> <Esc><C-w>h
inoremap <A-j> <Esc><C-w>j
inoremap <A-k> <Esc><C-w>k
inoremap <A-l> <Esc><C-w>l
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l

" Window Management
" Create Splits
nnoremap <A-v> <C-w>v:terminal<return>i
nnoremap <A-s> <C-w>s:terminal<return>i
inoremap <A-v> <Esc><C-w>v:terminal<return>i
inoremap <A-s> <Esc><C-w>s:terminal<return>i
tnoremap <A-v> <C-\><C-n><C-w>v:terminal<return>i
tnoremap <A-s> <C-\><C-n><C-w>s:terminal<return>i

" Move Splits -- Not working ideally... no concept of rotation...
nnoremap <A-{> <C-w>R
nnoremap <A-}> <C-w>r
tnoremap <A-{> <C-\><C-n><C-w>R
tnoremap <A-}> <C-\><C-n><C-w>r
" Abort Trap 6???
" nnoremap <A-_> <C-w>J
" nnoremap <A--> <C-w>K
" tnoremap <A-_> <C-\><C-n><C-w>J
" tnoremap <A--> <C-\><C-n><C-w>K
" bind -n      M-{ swap-pane -U
" bind -n      M-} swap-pane -D

" Resize Splits
nnoremap <silent> = :wincmd =<CR>

nnoremap <A-H> <C-w><
nnoremap <A-J> <C-w>+
nnoremap <A-K> <C-w>-
nnoremap <A-L> <C-w>>
inoremap <A-H> <Esc><C-w><
inoremap <A-J> <Esc><C-w>+
inoremap <A-K> <Esc><C-w>-
inoremap <A-L> <Esc><C-w>>
tnoremap <A-H> <C-\><C-n><C-w><
tnoremap <A-J> <C-\><C-n><C-w>+
tnoremap <A-K> <C-\><C-n><C-w>-
tnoremap <A-L> <C-\><C-n><C-w>>


" Session Commands
nnoremap <A-g> :Prosession 
tnoremap <A-g> <C-\><C-n>:Prosession 
