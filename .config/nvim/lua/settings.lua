local set = vim.o    -- set expandtab, et al.

set.compatible = false

-- nvim-compe prereq
set.completeopt = 'menuone,noselect'

-- Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
-- delays and poor user experience.
set.updatetime = 300

-- Don't pass messages to |ins-completion-menu|.
set.shortmess = 'c'

-- Default status line...
-- Split window bar fillchars and highlight so the split bar looks nicer:)
set.fillchars = 'eob: ,stl:─,stlnc:─,vert:│'
set.laststatus = 2  -- 0=never,1=for 2+ windows,default is 2=always


set.inccommand = 'nosplit'  -- show substitution while typing
set.ignorecase = true
set.lazyredraw = true
set.modelines = 1 -- default is on and 5 lines
set.mouse = 'a'

set.showmode = false -- required for statusline customizations
set.swapfile = false
set.wrap = false
set.number = false
set.path = '**'



set.scrolloff = 1
set.sidescrolloff = 5

-- FileType Defaults
set.expandtab = true
set.shiftround = true -- round 3 spaces to nearest tabstop
set.shiftwidth = 2
set.tabstop = 2

-- set['t_Co'] = '256'
set.background = 'dark'
set.termguicolors = true

set.smartcase = true
set.splitbelow = true
set.splitright = true


-- Neovide Settings
set.guifont = 'VictorMono Nerd Font:h13'
