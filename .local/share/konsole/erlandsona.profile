[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=erlandsona
Font=VictorMono Nerd Font,13,-1,5,57,0,0,0,0,0,Medium
LineSpacing=0
UseFontLineChararacters=false

[Cursor Options]
CursorShape=0

[General]
Icon=utilities-terminal
Name=erlandsona
Parent=FALLBACK/
TerminalCenter=true
TerminalMargin=0

[Scrolling]
HistoryMode=1
ScrollBarPosition=2

[Terminal Features]
BidiRenderingEnabled=true
BlinkingCursorEnabled=false
