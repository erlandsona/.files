# vim:foldmethod=marker:foldlevel=0

# {{{ Shell Options for Bash Version 4+ ########################################
#
# Include ctrl-s for i-search (forward through history)
# stty -ixon

shopt -s autocd
shopt -s cdspell
shopt -s direxpand
shopt -s dirspell
shopt -s globstar
# append to bash_history if Terminal.app quits
shopt -s histappend
set +f
# }}} ##########################################################################

# {{{ Language Exports #########################################################
export VISUAL=nvim
export EDITOR="$VISUAL"

### For Homebrew
export HOMEBREW_GITHUB_API_TOKEN=2c4758428d5646ea1aaf1703d7edeffc11cd052b

export PATH="/usr/local/sbin:$PATH"
export PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH"
export MANPATH="${HOME}/.linuxbrew/share/man:${MANPATH}"
export INFOPATH="${HOME}/.linuxbrew/share/info:${INFOPATH}"
###
# - For Stack/Yesod binaries
# - Elm Format
# - AWS heptio-authenticator for Kubernetes
export PATH="$HOME/.local/bin:$PATH"

# For Haskell
export PATH="$HOME/Library/Haskell/bin:$PATH"

# For installing PHP 7.3.2
export PATH="/usr/local/opt/bison/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/bison/lib"
export LDFLAGS="-L/usr/local/opt/zlib/lib"
export CPPFLAGS="-I/usr/local/opt/zlib/include"

# Ruby
export PATH="/usr/local/opt/ruby/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/ruby/lib"
export CPPFLAGS="-I/usr/local/opt/ruby/include"
export PKG_CONFIG_PATH="/usr/local/opt/ruby/lib/pkgconfig"

export PATH="/usr/local/opt/node@8/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/node@8/lib"
export CPPFLAGS="-I/usr/local/opt/node@8/include"
# }}}


# {{{ History VARS #############################################################
#
# Erase duplicates
# Bash History
export HISTCONTROL=ignoreboth
# resize history size
export HISTSIZE=500
export HISTFILESIZE=100000
export AUTOFEATURE=true autotest
# }}} ##########################################################################

# {{{ Aliases #################################################################

# Directory Movements
alias ..='cd ..'         # Go up one directory
alias ...='cd ../..'     # Go up two directories
alias ....='cd ../../..' # Go up three directories
alias -- -='cd -'        # Go back

# Unix Tools - Colors
# export LSCOLORS='Gxfxcxdxdxegedabagacad' # Default
export LSCOLORS='FxExCxDxBxegedabagaced'
export GREP_COLOR='1;33'
alias grep='grep --color=auto'

# ls
if [[ $(command -v gls) ]]; then
  alias ll="gls -Fla --color --group-directories-first"
  alias l1="gls -F1a --color --group-directories-first"
  alias ls="gls -Fha --color --group-directories-first"
elif [[ -n $(echo $OSTYPE | grep linux) ]]; then
  alias ll="ls -Fla --color --group-directories-first"
  alias l1="ls -F1a --color --group-directories-first"
  alias ls="ls -Fha --color --group-directories-first"
else
  alias ll="ls -FlaG"
  alias l1="ls -F1aG"
  alias ls="ls -FhaG"
fi

# Editor
alias nv="$EDITOR"
# alias vim="$EDITOR"

# pbcopy / pbpaste 4 linux
case $OSTYPE in
  linux*)
    # to use: install xclip
    XCLIP=$(command -v xclip)
    XSEL=$(command -v xsel)
    if [[ $XCLIP ]]; then
      alias pbcopy="$XCLIP -selection clipboard"
      alias pbpaste="$XCLIP -selection clipboard -o"
    elif [[ $XSEL ]]; then
      alias pbcopy="$XSEL --clipboard"
      alias pbpaste="$XSEL --output --clipboard"
    fi
    ;;
esac

# Git
# git bare repository for dotfiles...
alias dot="git --git-dir=$HOME/.files --work-tree=$HOME"

# Tree
if [[ ! $(command -v tree) ]]; then
  alias tree="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
fi

# }}} ##########################################################################

# {{{ Theme
normal="\e[0m"

SHELL_SSH_CHAR=""
SHELL_THEME_PROMPT_COLOR=43
SHELL_THEME_PROMPT_COLOR_SUDO=202

THEME_PROMPT_START=""
THEME_PROMPT_SEPARATOR=""
THEME_PROMPT_END=""
PROMPT_CHAR=" "

CWD_THEME_PROMPT_COLOR=234
CWD_THEME_TEXT_COLOR=85

LAST_STATUS_THEME_PROMPT_COLOR=52

function set_rgb_color {
  if [[ "${1}" != "-" ]]; then
    fg="38;5;${1}"
  fi
  if [[ "${2}" != "-" ]]; then
    bg="48;5;${2}"
    [[ -n "${fg}" ]] && bg=";${bg}"
  fi
  echo -e "\[\033[${fg}${bg}m\]"
}

function powerline_shell_prompt {
  SHELL_PROMPT_COLOR=${SHELL_THEME_PROMPT_COLOR}
  if sudo -n uptime 2>&1 | grep -q "load"; then
    SHELL_PROMPT_COLOR=${SHELL_THEME_PROMPT_COLOR_SUDO}
  fi
  if [[ -n "${SSH_CLIENT}" ]]; then
    SHELL_PROMPT="${SHELL_SSH_CHAR}\u@\h"
  else
    SHELL_PROMPT=""
  fi
  SHELL_PROMPT="\[\033[48;0;0;38;5;${SHELL_PROMPT_COLOR}m\]${THEME_PROMPT_START}\[\033[38;5;17;48;5;${SHELL_PROMPT_COLOR}m\] ${normal}"
  LAST_THEME_COLOR=${SHELL_PROMPT_COLOR}
}

function powerline_cwd_prompt {
  CWD_PROMPT="$(set_rgb_color ${LAST_THEME_COLOR} ${CWD_THEME_PROMPT_COLOR})${THEME_PROMPT_SEPARATOR}${normal}\[\033[38;5;85;48;5;234m\] \w ${normal}$(set_rgb_color ${CWD_THEME_PROMPT_COLOR} -)${normal}"
  LAST_THEME_COLOR=${CWD_THEME_PROMPT_COLOR}
}

function powerline_last_status_prompt {
  if [[ "$1" -eq 0 ]]; then
    LAST_STATUS_PROMPT="$(set_rgb_color ${LAST_THEME_COLOR} -)${THEME_PROMPT_END}${normal}"
  else
    LAST_STATUS_PROMPT="$(set_rgb_color ${LAST_THEME_COLOR} ${LAST_STATUS_THEME_PROMPT_COLOR})${THEME_PROMPT_SEPARATOR}${normal}$(set_rgb_color - ${LAST_STATUS_THEME_PROMPT_COLOR}) ${LAST_STATUS} ${normal}$(set_rgb_color ${LAST_STATUS_THEME_PROMPT_COLOR} -)${THEME_PROMPT_END}${normal}"
  fi
}

function powerline_prompt_command() {
  local LAST_STATUS="$?"
  local MOVE_CURSOR_RIGHTMOST='\033[500C'

  powerline_shell_prompt
  powerline_cwd_prompt
  powerline_last_status_prompt LAST_STATUS

    # Bash prompt expansion syntax
    # PS1='\[\e[31m\]\u\[\e[0m\]@\[\e[31m\]\h \[\e[36m\]\w \[\e[37m\]\$ \[\e[0m\]'
    # Rendering prompt at bottom causes scrollback issues with TMUX
    # \[$(retval=$?;tput cup "$LINES";exit $retval)\]
    PS1="\[$(retval=$?;exit $retval)\]${SHELL_PROMPT}${CWD_PROMPT}${LAST_STATUS_PROMPT}${MOVE_CURSOR_RIGHTMOST}\n${PROMPT_CHAR}"
}

PROMPT_COMMAND=powerline_prompt_command
# }}} ##########################################################################

# {{{ Integrations, source[s] and such
if [[ -f ~/.asdf/asdf.sh ]]; then
  source ~/.asdf/asdf.sh # ASDF Replaces [rb,php,py]env & [n,r]vm managers
fi
if [[ -f ~/.fzf.bash ]]; then
  source ~/.fzf.bash
fi
if [[ -f ~/.gpg.tokens ]]; then
  source ~/.gpg.tokens
fi
# }}} ##########################################################################

if [ -e /home/erlandsona/.nix-profile/etc/profile.d/nix.sh ]; then . /home/erlandsona/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
. "$HOME/.cargo/env"
