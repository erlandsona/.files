local wezterm = require('wezterm')

return {
  font = wezterm.font"VictorMono Nerd Font",
  font_size = 13,
  harfbuzz_features = { "onum=1" }, -- 123579812340098
  -- color_scheme = "3024 Day",
  -- color_scheme = "CLRS",
  color_scheme = require('colors').current,
  -- color_scheme = "Spacedust",
  -- color_scheme = "Twilight",
  color_schemes = require('colors').schemes,
  hide_tab_bar_if_only_one_tab = true,
  window_decorations = 'RESIZE',
  keys = {
    {key="Enter", mods="SUPER", action="ToggleFullScreen"},
    {key="F11", action="ToggleFullScreen"},
  },
}
