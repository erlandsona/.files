# vim:foldmethod=marker:foldlevel=0
# {{{ Shell Options for Zsh
#
# Include ctrl-s for i-search (forward through history)
# stty -ixon

bindkey -e # Tell Zsh not to be smart about EDITOR
# Fix broken delete key in Zsh?
bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

setopt AUTO_CD
# }}}

# {{{ History Config #############################################################
#
# set history size
export HISTSIZE=500
export HISTCONTROL=ignoreboth
export HISTFILE="$HOME/.zsh_history"
export HISTFILESIZE=100000
#save history after logout
# export SAVEHIST=10000

#save only one command if 2 common are same and consistent
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
# setopt SHARE_HISTORY             # Share history between all sessions.
#append into history file
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.

setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
# setopt HIST_BEEP                 # Beep when accessing nonexistent history.
# }}} ##########################################################################

# User configuration

# {{{ Aliases
# Directory Movements
alias ..='cd ..'         # Go up one directory
alias ...='cd ../..'     # Go up two directories
alias ....='cd ../../..' # Go up three directories
alias -- -='cd -'        # Go back

# Unix Tools - Colors
# export LSCOLORS='Gxfxcxdxdxegedabagacad' # Default
export LSCOLORS='FxExCxDxBxegedabagaced'
export GREP_COLOR='1;33'
alias grep='grep --color=auto'

# ls
if [[ $(command -v gls) ]]; then
  alias ll="gls -Fla --color --group-directories-first"
  alias l1="gls -F1a --color --group-directories-first"
  alias ls="gls -Fha --color --group-directories-first"
elif [[ -n $(echo $OSTYPE | grep linux) ]]; then
  alias ll="ls -Fla --color --group-directories-first"
  alias l1="ls -F1a --color --group-directories-first"
  alias ls="ls -Fha --color --group-directories-first"
else
  alias ll="ls -FlaG"
  alias l1="ls -F1aG"
  alias ls="ls -FhaG"
fi

# pbcopy / pbpaste 4 linux
case $OSTYPE in
  linux*)
    # to use: install xclip
    XCLIP=$(command -v xclip)
    XSEL=$(command -v xsel)
    if [[ $XCLIP ]]; then
      alias pbcopy="$XCLIP -in -selection clipboard"
      alias pbpaste="$XCLIP -selection clipboard -o"
    elif [[ $XSEL ]]; then
      alias pbcopy="$XSEL --clipboard"
      alias pbpaste="$XSEL --output --clipboard"
    fi
    ;;
esac

# Git
# git bare repository for dotfiles...
alias conf="git --git-dir=$HOME/.files --work-tree=$HOME"

# Tree
if [[ ! $(command -v tree) ]]; then
  alias tree="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
fi

# Tmux
alias tmls="tmux ls"
alias tml="tmux ls"
alias tmn="tmux new -s"
alias tmat="tmux a -t"
alias tma="tmux a"


alias startn="yarn && yarn bs:build && yarn dev"

# Mackey Aliases
gc () {
  # container=${1}
  # docker-compose exec -u root $container bash -l
  docker compose exec -u root mackey bash -lc "su mackey -c 'bin/mys mackey'"
}

# }}}

# {{{ Integrations, source[s] and such
# source $(brew --prefix asdf)/asdf.sh
if [[ -f ~/.asdf/asdf.sh ]]; then
  source ~/.asdf/asdf.sh # ASDF Replaces [rb,php,py]env & [n,r]vm managers
fi
if [[ -f ~/.fzf.zsh ]]; then
  source ~/.fzf.zsh
  export FZF_DEFAULT_COMMAND='rg --files --hidden --smartcase --glob "!.git/*" -C2'
fi
if [[ -f ~/.gpg.tokens ]]; then
  source ~/.gpg.tokens
fi
# }}}

# {{{ Simple Prompt
function powerline_precmd() {
  # Check exit code of last command
  last_code=$?
  exit_code=""
  if [[ $last_code != 0 ]]; then
    exit_code="%{%F{16}%K{1}%} $last_code $color_normal"
  fi

  color_normal="%{%F{foreground}%K{normal}%}"

  # Check if Sudo?
  if [[ $UID == 0 || $EUID == 0 ]]; then
    color_normal="%{%F{5}%K{normal}%}"
  fi
  current_directory="╭${${$(dirs)//\//"%{%F{4}/%F{foreground}%}"}//\~/  }"
  # Check if Sudo?
  git_line=""
  git status -sb \# &>/dev/null
  if [[ $? == 0 ]]; then
    git_status=$(git status -sb)
    git_line=${$(echo $git_status | grep \#)/\#/"%{%F{2}%}"}
    git_line=${$(echo $git_line)/.../"$color_normal...%{%F{1}%}"}
    git_line=${$(echo $git_line)/origin*/"origin$color_normal"}

    git_modified=$(echo $git_status | grep -c M)
    git_deleted=$(echo $git_status | grep -c D)
    git_conflicted=$(echo $git_status | grep -c C)
    git_untracked=$(echo $git_status | grep -c "\?\?")
  fi
  PS1="$color_normal$current_directory $git_line$color_normal
╰ $exit_code $color_normal"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi
# }}}

# # {{{ Powerline_Shell -  Python
# function powerline_precmd() {
#     PS1="$(powerline-shell --shell zsh $?)"
# }

# function install_powerline_precmd() {
#   for s in "${precmd_functions[@]}"; do
#     if [ "$s" = "powerline_precmd" ]; then
#       return
#     fi
#   done
#   precmd_functions+=(powerline_precmd)
# }

# if [ "$TERM" != "linux" ]; then
#     install_powerline_precmd
# fi
# # }}}

# # {{{ Powerline HS
# function powerline_precmd() {
#     PS1="$(powerline-hs zsh left)"
# }

# function install_powerline_precmd() {
#   for s in "${precmd_functions[@]}"; do
#     if [ "$s" = "powerline_precmd" ]; then
#       return
#     fi
#   done
#   precmd_functions+=(powerline_precmd)
# }

# if [ "$TERM" != "linux" ]; then
#     install_powerline_precmd
# fi

# # }}}

# {{{ Language Exports
export VISUAL=nvim
export EDITOR="$VISUAL"

### For Homebrew
export HOMEBREW_GITHUB_API_TOKEN=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

export PATH="/usr/local/sbin:$PATH"

###
# - For Stack/Yesod binaries
# - AWS heptio-authenticator for Kubernetes
export PATH="$HOME/.local/bin:$PATH"

# For Haskell on OSX
export PATH="$HOME/Library/Haskell/bin:$PATH"

# For Erlang on OSX
if [[ $(command -v brew) ]]; then
  export KERL_CONFIGURE_OPTIONS="--without-javac --disable-hipe --with-ssl=$(brew --prefix openssl)"
  export ERL_AFLAGS="-kernel shell_history enabled"
  export NODEJS_CHECK_SIGNATURES=no
fi

# For installing PHP 7.3.2
# export PATH="/usr/local/opt/bison/bin:$PATH"
# export LDFLAGS="-L/usr/local/opt/bison/lib"
# export LDFLAGS="-L/usr/local/opt/zlib/lib"
# export CPPFLAGS="-I/usr/local/opt/zlib/include"

# Ruby
export PATH="/usr/local/opt/ruby/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/ruby/lib"
export CPPFLAGS="-I/usr/local/opt/ruby/include"
export PKG_CONFIG_PATH="/usr/local/opt/ruby/lib/pkgconfig"

# Node
export PATH="/usr/local/opt/node@14/bin:$PATH"
NPM_PACKAGES="${HOME}/.npm-packages"
export PATH="$NPM_PACKAGES/bin:$PATH"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

# Yarn
# export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

# opam configuration
test -r ~/.opam/opam-init/init.zsh && source ~/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# Rust
source $HOME/.cargo/env
# export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/usr/local/opt/make/libexec/gnubin:$PATH"

# Nix
if [ -e /home/erlandsona/.nix-profile/etc/profile.d/nix.sh ]; then . /home/erlandsona/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
