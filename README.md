For a fresh installation set the `conf` alias for the current shell

```
mkdir ~/.files && git clone --bare git@bitbucket.org:erlandsona/.files ~/.files && git checkout -f
alias conf='/usr/bin/git --git-dir=$HOME/.files/ --work-tree=$HOME'
conf config --local status.showUntrackedFiles no
```


### Install Homebrew / Use whatever package manager is available

```
brew install neovim
brew install coreutils # for gls which is nice ls directory formatting.
```

### Run `:checkhealth` in NeoVim and it'll tell you what to do.

Also language support for other plugins is good too so run

in short

```
sudo gem install neovim
pip3 install --user neovim
npm i -g neovim
```

Then in vim

```
:PythonSupportInitPython2
:PythonSupportInitPython3
```
