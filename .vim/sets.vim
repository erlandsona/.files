" {{{ Sets

" Already NVim Defaults
" set autoindent " default on
" set autoread " default on
" set backspace=indent,eol,start

" UNCOMMENT: NOTE: Not sure this is necessary?
" set backup writebackup
" set backupcopy=yes
" set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
" set backupskip=/tmp/*,/private/tmp/*
" set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp

" Potential perf improvement?
" let g:clipboard = {'copy': {'+': 'pbcopy', '*': 'pbcopy'}, 'paste': {'+': 'pbpaste', '*': 'pbpaste'}, 'name': 'pbcopy', 'cache_enabled': 0}
" set clipboard+=unnamedplus
" set clipboard=unnamed

" UNCOMMENT Maybe???
" Remove 'Press Enter to continue' message when type information is longer than one line.
" set cmdheight=2 " default is one in nvim to remove 'Press Enter to continue' messages
" set complete-=i
" set encoding=utf-8

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Default status line...
" Split window bar fillchars and highlight so the split bar looks nicer:)
set fillchars=eob:\ ,stl:─,stlnc:─,vert:│
set laststatus=2  " 0=never,1=for 2+ windows,default is 2=always

" UNCOMMENT: Consider making this marker cause it's all you use.
" set foldenable
" set foldlevel=10
" set foldlevelstart=10
" set foldnestmax=10
" set foldmethod=indent

" set guicursor=  " Fixes `sudo nvim /file/path` from outputing spurios q's

" set hlsearch " default on
" set incsearch " default on
set inccommand=nosplit  " show substitution while typing
set ignorecase
set lazyredraw
set modelines=1 " default is on and 5 lines
set mouse=a
" set mousefocus " only works in a GUI

" Defaults to off and nvim doesn't have t_xx options
" set noerrorbells visualbell t_vb=
" if has('autocmd')
"   autocmd GUIEnter * set visualbell t_vb=
" endif


" Default is off or nohidden
" set nohidden " Omnisharp? - Don't hide buffers causing them to build up in memory. Instead when leaving a file with changes decide whether or not to save those changes.
" set hidden " ctrl-space - Don't ask to save when changing buffers (i.e. when jumping to a type definition)

set noshowmode " required for statusline customizations
set noswapfile
set nowrap
set nonumber
set path+=**

set scrolloff=1
set sidescrolloff=5

" FileType Defaults
set expandtab
set shiftround " round 3 spaces to nearest tabstop
set shiftwidth=2
set tabstop=2

" set showmatch
" set noshowmatch " Suggested by omnisharp-vim.
set smartcase
" set smarttab " defaults to on
set splitbelow
set splitright

" UNCOMMENT: supposed to make highlihting faster?
" set synmaxcol=300
" syntax sync minlines=300
" }}}
" vim:foldmethod=marker:foldlevel=0
