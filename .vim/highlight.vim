syntax on
colorscheme peachpuff

" Make peachpuff w/ termguicolors the same as without.
hi Normal guibg=none guifg=none

" {{{ Vim colors.
hi Comment cterm=italic gui=italic
hi DiffAdd cterm=none ctermfg=10 ctermbg=none guifg=g:terminal_color_10 guibg=green gui=none
hi DiffDelete cterm=none ctermfg=9 ctermbg=none guifg=g:terminal_color_9 guibg=red gui=none
hi DiffChange cterm=none ctermfg=13 ctermbg=none guifg=g:terminal_color_13 guibg=g:terminal_color_3 gui=none
hi DiffText cterm=none ctermfg=208 ctermbg=none guifg=bg guibg=g:terminal_color_6 gui=none
hi VertSplit term=none cterm=none ctermfg=8 ctermbg=none guibg=none guifg=g:terminal_color_3
hi StatusLine term=none cterm=none ctermfg=8 ctermbg=none guibg=none guifg=g:terminal_color_3
hi StatusLineNC term=none cterm=none ctermfg=8 ctermbg=none guibg=none guifg=g:terminal_color_3 gui=none
hi Search cterm=none ctermfg=none ctermbg=black gui=underline,bold guifg=none guibg=#222222
hi LineNr ctermfg=5 guifg=g:terminal_color_5
hi CursorLine cterm=underline gui=underline guibg=none
hi CursorLineNr ctermfg=11 guifg=11
hi Folded ctermfg=4 ctermbg=7 guifg=g:terminal_color_3 guibg=bg

" Tabline
hi TabLineFill cterm=none gui=none
hi TabLine cterm=none ctermfg=0 ctermbg=7 gui=none guifg=bg guibg=fg
hi TabLineSel cterm=bold,underline gui=bold,underline guifg=bg guibg=fg

" NERDTree
hi Directory ctermfg=4 guifg=g:terminal_color_4


" PopUp Menus
hi Pmenu      ctermfg=0 ctermbg=none guibg=#222222
hi PmenuSel   ctermfg=0 ctermbg=7 guibg=Grey
hi PmenuSbar  ctermbg=248 guibg=Grey
hi PmenuThumb ctermbg=0 guibg=Black
" }}}

" {{{ Highlight Links
hi! link multiple_cursors_cursor Search
hi! link multiple_cursors_visual Search
hi! link Visual Search
" }}}

hi SignColumn ctermbg=none guibg=none

hi GitSignsChange guifg=#bbbb00 guibg=bg gui=none
hi GitSignsAdd guifg=#008000 guibg=bg gui=none
hi GitSignsDelete guifg=#ff0000 guibg=bg gui=none

" highlight line 80 and 120+
hi ColorColumn ctermbg=black guibg=black
let &colorcolumn="80,".join(range(120,999),",")

" 80 Column - Not working because it conflicts with vim-better-whitespace
" hi OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
" function! OverLength()
"   3match OverLength /\%>80v.\+/
" endfunction

" call OverLength()
" augroup OverLength
"   autocmd!
"   autocmd BufEnter,BufWrite
"         \,CursorMoved,CursorMovedI
"         \,InsertEnter,InsertLeave
"         \ cs
"         \,elm,haskell
"         \,javascript,javascript.jsx,js
"         \,json
"         \,php
"         \,python
"         \,ruby
"         \,vim
"         \ call OverLength()
" augroup END

" vim:foldmethod=marker:foldlevel=0
