hi! Modified  guifg=red ctermfg=red
hi! BaseColor guibg=none ctermbg=none

function! RedrawStatuslineColors()
  let guicolor = get({
        \ 'n': g:terminal_color_4,
        \ 'i': g:terminal_color_5,
        \ 'R': g:terminal_color_1,
        \ 'v': g:terminal_color_2,
        \ 'V': g:terminal_color_2,
        \ '': g:terminal_color_2
        \ }, mode(), g:terminal_color_0)

  let ctermcolor = get({
        \ 'n': 'darkblue',
        \ 'i': 'darkmagenta',
        \ 'R': 'darkred',
        \ 'v': 'darkgreen',
        \ 'V': 'darkgreen',
        \ '': 'darkgreen'
        \ }, mode(), 'black')

  let bg = 7 " grey
  exec  "hi! PathColor guibg=".guicolor." ctermbg=".ctermcolor
    \ "| hi! IconColor guibg=".bg." guifg=".guicolor." ctermbg=".bg." ctermfg=".ctermcolor
  return ""
endfunction

function! StatusLineEnter()
  let statusline="%{RedrawStatuslineColors()}"
  let statusline.="%#BaseColor# %#PathColor#%#PathColor#%< %f %#IconColor#"
  let statusline.=&mod==1 ? "%#Modified#%m" : ""
  let statusline.="%R%*"

  if exists('b:term_title')
    return ""
  else
    return statusline
  endif
endfunction

function! StatusLineLeave()
  return "%#Modified#%m%#StatusLine#"
endfunction

set statusline=%!StatusLineEnter()
augroup statusline
  autocmd!
  au WinEnter * setlocal statusline=%!StatusLineEnter()
  au WinLeave * setlocal statusline=%!StatusLineLeave()
augroup END
