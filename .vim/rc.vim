" Austin Erlandson's .vimrc

set nocompatible

" Auto install vim-plug
let plug_path = has('nvim') ?
      \ '~/.local/share/nvim/site/' :
      \ '~/.vim/'

if empty(glob(plug_path . "autoload/plug.vim"))
  execute "! curl -fLo " . plug_path . "autoload/plug.vim --create-dirs"
    \ . " https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  augroup plug_install
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  augroup end
endif

" {{{ Plugins
call plug#begin()

" {{{ Syntax...
Plug 'sheerun/vim-polyglot'
" Plug 'elzr/vim-json', { 'for': 'json' } " JSON Syntax Highlighting
" }}}
" {{{ LanguageClient-nvim
" Plug 'neovim/nvim-lspconfig'
" }}}

" {{{ coc.nvim
Plug 'neoclide/coc.nvim', {'branch': 'release'}
let g:coc_global_extensions = ['coc-json']

" }}}

" {{{ ALE
" " let g:ale_completion_enabled = 1 " Breaks deoplete completion
" Plug 'w0rp/ale' " Async Linting Engine... successor to syntastic?
" let g:ale_linters = {
" \  'javascript': ['eslint', 'tsserver'],
" \  'typescript': ['eslint', 'tsserver'],
" \  'elixir': ['elixir-ls'],
" \  'elm': ['elm_ls']
" \}
" let g:ale_fixers = {
" \  '*': ['remove_trailing_lines', 'trim_whitespace'],
" \  'elm': ['elm-format'],
" \  'javascript': ['prettier', 'eslint'],
" \  'typescript': ['prettier', 'eslint'],
" \  'elixir': ['mix_format'],
" \  'reason': ['refmt'],
" \}
" let g:ale_fix_on_save = 1
" let g:ale_elixir_elixir_ls_release = '/Users/aerlandson/.local/bin'
" " let g:ale_set_highlights = 0
" let g:ale_reason_ls_executable = 'reason-language-server'
" let g:ale_reasonml_refmt_executable = 'bsrefmt'

" let g:ale_typescript_prettier_use_local_config = 1

" let g:ale_fix_on_save = 1
" " let g:ale_linters_explicit = 1
" }}}

" {{{ Completion
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" let g:deoplete#enable_at_startup = 1
" func! Multiple_cursors_before()
"   if deoplete#is_enabled()
"     call deoplete#disable()
"     let g:deoplete_is_enable_before_multi_cursors = 1
"   else
"     let g:deoplete_is_enable_before_multi_cursors = 0
"   endif
" endfunc
" func! Multiple_cursors_after()
"   if g:deoplete_is_enable_before_multi_cursors
"     call deoplete#enable()
"   endif
" endfunc
" }}}

Plug 'terryma/vim-multiple-cursors'

" Plug 'airblade/vim-gitgutter'
" let g:gitgutter_max_signs = 300
" GitGutter is slow???
Plug 'mhinz/vim-signify'
let g:signify_vcs_list = ['git']
let g:signify_sign_change = '~'

" Plug 'justinmk/vim-dirvish' " Just broken for some reason?
" {{{ Netrw
" Plug 'tpope/vim-vinegar'
" " let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro' " defaults
" let g:netrw_cursor = 2
" let g:netrw_fastbrowse = 0 " Cache stuff, manually refresh <ctrl>-l to see new files.
" let g:netrw_liststyle = 1 " ls -la
" let g:netrw_localrmdir="rm -r"
" let g:netrw_banner = 0 " Turn off banner
" let g:netrw_browse_split = 0 " Default but set to avoid overrides
" " Sort Directories next to similarly named Files
" let g:netrw_sort_sequence='\<core\%(\.\d\+\)\=\>,\.h$,\.c$,\.cpp$,\~\=\*$,*,\.o$,\.obj$,\.info$,\.swp$,\.bak$,\~$'
"
" " for :SudoWrite, and friends
" Plug 'tpope/vim-eunuch' " unneeded w/ NerdTree
" }}}

" {{{ NerdTree
Plug 'dhruvasagar/vim-vinegar', { 'as': 'nerd-vinegar' }
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
" let NERDTreeChDirMode = 0 " Don't change Vim's CWD
let NERDTreeHijackNetrw = 1
let NERDTreeHighlightCursorline = 1
let NERDTreeIgnore = ['DS_Store']
let NERDTreeMouseMode = 3 " Single click any node to open.
let NERDTreeShowHidden = 1
let NERDTreeShowLineNumbers = 0
let NERDTreeCascadeOpenSingleChildDir = 1
let NERDTreeAutoDeleteBuffer = 1
" }}}

Plug 'stefandtw/quickfix-reflector.vim' " Make edits to entries in the quickfix window
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
" for :DB postgres://etc... running sql queries from vim...
Plug 'tpope/vim-dadbod'

" Blocks / Text Objects
Plug 'andymass/vim-matchup'


" Plug 'tpope/vim-projectionist'

" Plug 'thaerkh/vim-workspace'
" Plug 'airblade/vim-rooter'
Plug 'tpope/vim-obsession'
  Plug 'dhruvasagar/vim-prosession'
    " Plug 'gikmx/vim-ctrlposession'
" source ~/.vim/sessions.vim

" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'

"###########################
" Vim-Test
"###########################
" UNCOMMENT
" Plug 'janko-m/vim-test'

" Plug 'ludovicchabant/vim-gutentags'

" UNCOMMENT
" Plug 'majutsushi/tagbar'

" Might be slow...
Plug 'Raimondi/delimitMate' " ' => ''

Plug 'wellle/targets.vim' " Extra Text Objects
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-indent'
Plug 'kana/vim-textobj-line'
Plug 'andyl/vim-textobj-elixir'
Plug 'Julian/vim-textobj-variable-segment' " av/iv for camelCase or snake_case parts...
" Plug 'nelstrom/vim-textobj-rubyblock', { 'for': ['ruby', 'erb', 'rspec'] }

" UNCOMMENT
Plug 'troydm/zoomwintab.vim'
let g:zoomwintab_hidetabbar=0
nnoremap <silent> <C-z> :ZoomWinTabToggle<CR>
"
" tnoremap <M-z> <C-\><C-n>:ZoomWinTabToggle<CR>i

Plug 'ntpeters/vim-better-whitespace'
call plug#end() " initialize plugin system
" }}}

source ~/.vim/sets.vim

let g:matchup_matchparen_status_offscreen = 0 " Turn off statusline override

" Leader
let mapleader = ','

" {{{ Auto Commands
augroup ft_overrides
  autocmd!

  " autocmd CursorHoldI * :call <SID>show_hover_doc()
  " autocmd CursorHold * :call <SID>show_hover_doc()


  " shiftwidth&tabstop=2
  " autocmd BufNewFile,BufRead *.axlsx setlocal filetype=ruby
  " autocmd FileType html,ruby,rspec,eruby,yml,yaml,javascript,javascript.jsx,json,cson,js,coffee,cabal setlocal shiftwidth=2 tabstop=2
  " shiftwidth&tabstop=4
  " autocmd BufRead,BufNewFile *.eliom,*.eliomi setlocal filetype=ocaml
  " autocmd FileType elm,cs,markdown,sass,php,blade setlocal shiftwidth=4 tabstop=4

  " autocmd BufNewFile,BufRead Jenkinsfile setlocal filetype=groovy
  " autocmd BufNewFile,BufRead *.elm setlocal filetype=elm

  " add do/end as jumps for %
  autocmd FileType elixir,ruby let b:match_words = '\<\(do\|fn\)\>:\<end\>'
  autocmd FileType elm let b:match_words = '\<\(let\)\>:\<in\>'

  " autocmd FileType elixir,elm,ruby let b:delimitMate_nesting_quotes = ['"']


  autocmd BufRead,BufNewFile *.md setlocal linebreak wrap
  autocmd BufRead,BufNewFile *.md hi! ColorColumn ctermbg=none



  " Format on Save for Elm
  " autocmd BufWritePost *.elm silent execute "!elm-format --yes %"

augroup end

" }}}

source ~/.vim/highlight.vim

" source ~/.vim/mappings.vim
" {{{ Mappings
nnoremap ; :

" {{{ lspconfig
" lua <<EOF
"   local lspconfig = require 'lspconfig'
"   lspconfig.elmls.setup({})
" EOF
" }}}

" {{{ Ale
" nmap <silent> <leader>= <Plug>(ale_fix)
" nmap <silent> <leader>g <Plug>(ale_go_to_definition)
" nmap <silent> <leader>gs <Plug>(ale_go_to_definition_in_split)
" nmap <silent> <leader>gv <Plug>(ale_go_to_definition_in_vsplit)
" nmap <silent> <leader>G <Plug>(ale_go_to_type_definition)
" nmap <silent> <leader>GS <Plug>(ale_go_to_type_definition_in_split)
" nmap <silent> <leader>GV <Plug>(ale_go_to_type_definition_in_vsplit)
" }}}

" " {{{ coc.nvim

" " Use tab for trigger completion with characters ahead and navigate.
" " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" " other plugin before putting this into your config.
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? "\<C-n>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" Keybindings

" Not supported by elm-language-server?
" nmap <silent> <leader>t <Plug>(coc-type-definition)
" nmap <silent> <leader>g <Plug>(coc-implementation)
" nmap <leader>r <Plug>(coc-rename)
nmap <silent> <leader>r <Plug>(coc-references)
nmap <silent> <leader>s <Plug>(coc-fix-current)
nmap <silent> <leader>S <Plug>(coc-codeaction)
nmap <silent> <leader>a <Plug>(coc-diagnostic-next)
nmap <silent> <leader>A <Plug>(coc-diagnostic-next-error)
nmap <silent> <leader>g <Plug>(coc-definition)
nmap <silent> <leader>h :call CocActionAsync('doHover')<CR>
nmap <silent> <leader>= :call CocActionAsync('format')<CR>


" Elm Format is running really slow on larger files...
autocmd BufWritePre *.elm call CocAction('format')
" autocmd BufWritePost *.elm execute "!elm-format --yes %" | redraw!
" }}}


" {{{ FZF, CtrlP, Ag, Etc...
" nnoremap <c-t> :Tags<CR>
nnoremap <c-p> :GFiles -co --exclude-per-directory=.gitignore<CR>
nnoremap <c-\> :Rg <c-r><c-w><CR>
vnoremap <c-\> "vy:Rg <c-r>v<CR>
nnoremap <leader><bslash> :Rg<Space>

" override Ag and Rg commands to search inside git repo and add preview
command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>, fzf#vim#with_preview({'dir': FugitiveWorkTree()}), <bang>0)

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
    \ "rg --multiline --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>),
    \ 1,
    \ fzf#vim#with_preview({'dir': FugitiveWorkTree()}),
    \ <bang>0
  \ )

" override GFiles to add preview
command! -bang -nargs=? GFiles
    \ call fzf#vim#gitfiles(
    \ '-co --exclude-per-directory=.gitignore',
    \ fzf#vim#with_preview(),
    \ <bang>0
  \ )
" }}}

" {{{ Vim-Tmux-Navigator:
" `let g:tmux_navigator_disable_when_zoomed = 1` " Causes slow pane switching
" TmuxAwareNavigate is a refactor based on this .vimrc
function! TmuxAwareNavigate(to)
  let current_vim_pane = winnr()
  execute 'wincmd ' . a:to
  if current_vim_pane == winnr()
    " The socket path is the first value in the comma-separated list of $TMUX.
    let socket = split($TMUX, ',')[0]
    let selected_pane = 'select-pane -'.tr(a:to, 'phjkl', 'lLDUR')
    let cmd = 'tmux -S '.socket.' '.selected_pane
    return system(cmd)
    silent call s:TmuxCommand(args)
  endif
endfunction

nnoremap <silent> <M-h> :call TmuxAwareNavigate('h')<CR>
nnoremap <silent> <M-j> :call TmuxAwareNavigate('j')<CR>
nnoremap <silent> <M-k> :call TmuxAwareNavigate('k')<CR>
nnoremap <silent> <M-l> :call TmuxAwareNavigate('l')<CR>
inoremap <silent> <M-h> <Esc>:call TmuxAwareNavigate('h')<CR>
inoremap <silent> <M-j> <Esc>:call TmuxAwareNavigate('j')<CR>
inoremap <silent> <M-k> <Esc>:call TmuxAwareNavigate('k')<CR>
inoremap <silent> <M-l> <Esc>:call TmuxAwareNavigate('l')<CR>
" nnoremap <silent> <BS> :TmuxNavigateLeft<CR>
" nnoremap <Esc>\ :TmuxNavigatePrevious<CR>
" }}}

" {{{ Vim-Test
" nnoremap <silent> <leader>t :TestNearest<CR>
" nnoremap <silent> <leader>T :TestFile<CR>
" nnoremap <silent> <leader>a :TestSuite<CR>
" nnoremap <silent> <leader>l :TestLast<CR>
" nnoremap <silent> <leader>g :TestVisit<CR>
" }}}

" {{{ Reset Pane Layout
nnoremap <silent> = :wincmd =<CR>
" }}}

" {{{ Clear highlighting on escape in normal mode
nnoremap <silent> <leader>/ :noh<CR>
" }}}

" {{{ Set control-d/u to only scroll 5 lines
nnoremap <c-u> 5k
nnoremap <c-d> 5j
nnoremap <c-b> 20k
nnoremap <c-f> 20j
vnoremap <c-u> 5k
vnoremap <c-d> 5j
vnoremap <c-b> 20k
vnoremap <c-f> 20j
" }}}

" {{{ Set mouse scrolling more sensible.
map <ScrollWheelUp> <c-y>
map <ScrollWheelDown> <c-e>
" }}}

" {{{ Search for selected text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
" }}}
" }}}

source ~/.vim/statusline.vim

" vim:foldmethod=marker:foldlevel=0
